// imports
import { combineReducers } from 'redux'
import { reducer as reduxFormReducer } from 'redux-form'

// reducers
import player from './player'
import vote from './vote'

// and combined
export const reducer = combineReducers({
    form: reduxFormReducer,
    player,
    vote,
})
