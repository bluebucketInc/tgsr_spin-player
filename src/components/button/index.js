// imports
import React from 'react'

// styles
import './styles.scss'

// quiz button
export const Button = ({ onClick, children, classes, type }) => (
    <button
        className={`button raised ${classes ? classes : ''}`}
        onClick={onClick}
        type={type}>
        {children}
    </button>
)
