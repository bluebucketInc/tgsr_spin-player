// imports
import React from 'react'
import { withOrientationChange } from 'react-device-detect'

import './styles.scss'

// component
const OrientationMessage = ({ isPortrait }) => {
    if (isPortrait)
        return (
            <div className="orientation-wrapper animated fadeIn">
                <img
                    src={require('./assets/orientation-logo.png')}
                    alt="Please rotate your device"
                />
            </div>
        )
    else {
        return <React.Fragment></React.Fragment>
    }
}

// export
export default withOrientationChange(OrientationMessage)
