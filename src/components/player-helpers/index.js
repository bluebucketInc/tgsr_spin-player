// imports
import React from 'react'
import { Link } from 'react-router-dom'

// components
import { Button } from '../button'

// styles
import './styles.scss'

// component
const PlayerHelpers = ({ hasPlayed }) => {
    return (
        <React.Fragment>
            {/* Bottom Controls */}
            <div className="bottomctrls-container">
                <Link
                    to="/vote"
                    className={`wrapped-button ${
                        hasPlayed ? '' : 'disabled-link'
                    }`}>
                    <Button classes={hasPlayed ? '' : 'disabled'}>
                        Vote this mashup
                    </Button>
                </Link>
                <div className="helper-links">
                    <p>
                        <Link to="/why-vote">Why Vote?</Link>
                    </p>
                    <span>|</span>
                    <p>
                        <Link to="/how-to-play">How to Play?</Link>
                    </p>
                    <span>|</span>
                    <p>
                        <Link to="/how-to-win">How to Win Prizes?</Link>
                    </p>
                </div>
            </div>
        </React.Fragment>
    )
}

// export
export default PlayerHelpers
