// polyfill
import 'react-app-polyfill/ie11'

// imports
import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { BrowserRouter } from 'react-router-dom'

// components
import store from './state/store'
import App from './App'

// styles
import 'sanitize.css/sanitize.css'
import './styles/app.scss'

// render method
render(
    <Provider store={store}>
        <BrowserRouter>
            <App />
        </BrowserRouter>
    </Provider>,
    document.querySelector('#root')
)
