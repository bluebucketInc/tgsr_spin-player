// imports
import React from 'react'
import ReCAPTCHA from 'react-google-recaptcha'
import { Link } from 'react-router-dom'
// components
import FadeInWrapper from '../../components/fadein-wrapper'
import SubmitForm from '../../components/form'
import { Button } from '../../components/button'
import { IndextoGenre } from '../../utils'
import { siteKey } from '../../utils/settings'

// styles
import './styles.scss'

// ReCaptcha
export const ReCaptchaPage = ({
    left,
    right,
    onVoteSubmit,
    onCaptchaComplete,
    isCaptchaVerified,
}) => {
    // render
    return (
        <FadeInWrapper isVisible={true}>
            <h2>AWESOME CHOICE! YOU’RE VOTING FOR</h2>
            <div className="genre-wrapper">
                <h1>{IndextoGenre(left, 'left')}</h1>
                <img
                    className="genre-x"
                    src={require('./assets/close.png')}
                    alt="Mashup"
                />
                <h1>{IndextoGenre(right, 'right')}</h1>
            </div>
            <p>
                Just one more step before submitting your vote. Please verify
                below.
            </p>
            <div className="captcha-wrapper">
                <ReCAPTCHA sitekey={siteKey} onChange={onCaptchaComplete} />
            </div>
            <Button
                classes={`long-btn ${isCaptchaVerified ? '' : 'disabled'}`}
                onClick={() => onVoteSubmit(left, right)}>
                Submit
            </Button>
        </FadeInWrapper>
    )
}

// Social Share
export const SharePage = ({ left, right, onShare, onClose }) => {
    // render
    return (
        <FadeInWrapper isVisible={true}>
            <h2>THANK YOU FOR YOUR VOTE!</h2>
            <div className="genre-wrapper">
                <h1>{IndextoGenre(left, 'left')}</h1>
                <img
                    className="genre-x"
                    src={require('./assets/close.png')}
                    alt="Mashup"
                />
                <h1>{IndextoGenre(right, 'right')}</h1>
            </div>
            <p>
                Share your mashup to unlock your chances to win amazing prizes!
            </p>
            <p>
                <strong>
                    Should you win the weekly draw, you will be required to show
                    proof of posting at the point of prize collection.
                </strong>
            </p>
            <div className="button-group buttons-wrapper">
                <Button classes="long-btn" onClick={() => onShare('fb')}>
                    SHARE ON FACEBOOK
                </Button>
                <Button classes="long-btn" onClick={() => onShare('tw')}>
                    SHARE ON TWITTER
                </Button>
            </div>
            <Link to="/" className="link-para" onClick={onClose}>
                Create New Mashup
            </Link>
            <div className="scratchcard-container">
                <img
                    className="genre-bg"
                    src={require(`./assets/bg/${right}.png`)}
                    alt="TGSR"
                />
                <img
                    src={require(`./assets/scratch-card-locked.png`)}
                    alt="Scratch & Win"
                />
            </div>
        </FadeInWrapper>
    )
}

// Draw Page
export const DrawPage = ({ left, right, onNextStep, onClose, chances }) => {
    // render
    return (
        <FadeInWrapper isVisible={true}>
            <h2>THANK YOU FOR SHARING YOUR MASHUP!</h2>
            <div className="genre-wrapper">
                <h1>{IndextoGenre(left, 'left')}</h1>
                <img
                    className="genre-x"
                    src={require('./assets/close.png')}
                    alt="Mashup"
                />
                <h1>{IndextoGenre(right, 'right')}</h1>
            </div>
            <p>
                You've unlocked 20 chances to win in this week's draw. Enter the
                draw now!
            </p>
            <div className="buttons-wrapper">
                <Button classes="long-btn" onClick={() => onNextStep(4)}>
                    ENTER THE DRAW
                </Button>
            </div>
            <Link to="/" className="link-para" onClick={onClose}>
                Create New Mashup
            </Link>
            <div className="scratchcard-container">
                <img
                    className="genre-bg"
                    src={require(`./assets/bg/${right}.png`)}
                    alt="TGSR"
                />
                <img
                    src={require(`./assets/scratch-card-unlocked_${chances}.png`)}
                    alt="Scratch & Win"
                />
            </div>
        </FadeInWrapper>
    )
}

// form
export const FormPage = ({ onSubmit, chances }) => {
    // render
    return (
        <FadeInWrapper isVisible={true}>
            <h3 className="form-title">CONGRATULATIONS!</h3>
            <p>
                You’ve received {chances} chances to win $100 GV voucher in this
                week’s draw!
                <br />
                To enter the draw, simply submit your details. To enter the
                draw, submit your details. Winners will be contacted via the
                details provided.
            </p>
            <p>
                <strong>
                    Should you win the weekly draw, you will be required to show
                    proof of posting at the point of prize collection.
                </strong>
            </p>
            <SubmitForm onSubmit={onSubmit} />
        </FadeInWrapper>
    )
}

// Thank you
export const ThankYouKiosk = ({ left, right, onClose }) => {
    // render
    return (
        <FadeInWrapper isVisible={true}>
            <h2>THANK YOU FOR YOUR VOTE!</h2>
            <div className="genre-wrapper">
                <h1>{IndextoGenre(left, 'left')}</h1>
                <img
                    className="genre-x"
                    src={require('./assets/close.png')}
                    alt="Mashup"
                />
                <h1>{IndextoGenre(right, 'right')}</h1>
            </div>
            <p>
                You’ve voted for {IndextoGenre(left, 'left')} x{' '}
                {IndextoGenre(right, 'right')}. Simply approach any of our staff
                to collect
                <br />a scratch card and stand a chance to win amazing prizes!
            </p>
            <div className="buttons-wrapper">
                <Link to="/" onClick={onClose}>
                    <Button classes="long-btn" onClick={onClose}>
                        create new mashup
                    </Button>
                </Link>
            </div>
            <div className="scratchcard-container">
                <img
                    src={require('./assets/scratch-card-locked.png')}
                    alt="Scratch & Win"
                />
            </div>
        </FadeInWrapper>
    )
}

// Thank you
export const ThankYouDesktop = ({ left, right, onClose }) => {
    // render
    return (
        <FadeInWrapper isVisible={true}>
            <h3 className="form-title">GOOD LUCK!</h3>
            <p>
                We will get in touch if you're one of the three lucky winners.
                <br />
                Meanwhile, why not create more mashups to get more chances
            </p>
            <div className="buttons-wrapper">
                <Link to="/" onClick={onClose}>
                    <Button classes="long-btn" onClick={onClose}>
                        create new mashup
                    </Button>
                </Link>
            </div>
        </FadeInWrapper>
    )
}

// error page
export const ErrorPage = ({ onClose }) => {
    // render
    return (
        <FadeInWrapper isVisible={true}>
            <h3 className="form-title">WHOOPS!</h3>
            <p>Something went wrong. Please try again later.</p>
            <div className="buttons-wrapper">
                <Link to="/" onClick={onClose}>
                    <Button classes="long-btn" onClick={onClose}>
                        Back to home
                    </Button>
                </Link>
            </div>
        </FadeInWrapper>
    )
}
