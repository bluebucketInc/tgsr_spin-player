// imports
import React, { useRef } from 'react'
import { Link } from 'react-router-dom'
import Slider from 'react-slick'
import { isMobileOnly } from 'react-device-detect'

// modules
import { slickSettings } from '../../utils/settings'
import { onBoarding } from '../../data/onboarding'
import { Button } from '../../components/button'
import ModelWrapper from '../../components/modal-wrapper'

// styles
import './styles.scss'

// component
const OnboardingScreens = ({ onOnboardingComplete }) => {
    // reference
    const slider = useRef(null)
    // next slide
    const goNext = () => slider.current.slickNext()

    // render
    return (
        <React.Fragment>
            <ModelWrapper isActive={true} onClose={onOnboardingComplete} to="/">
                <div
                    className={`container onboarding-container ${
                        isMobileOnly ? 'mobile' : ''
                    }`}>
                    <Slider
                        ref={slider}
                        {...slickSettings}
                        className="onboarding-slider">
                        {onBoarding.map(item => (
                            <div className="slides" key={item.sub}>
                                <div className="content-wrapper">
                                    <h5>{item.sub}</h5>
                                    <h3>{item.head}</h3>
                                </div>
                                <img
                                    className="onboarding-media"
                                    src={
                                        isMobileOnly ? item.image_m : item.image
                                    }
                                    alt={item.head}
                                />
                                {item.end ? (
                                    <Link to="/" onClick={onOnboardingComplete}>
                                        <Button classes="onboarding-btn long-btn">
                                            START SPINNING
                                        </Button>
                                    </Link>
                                ) : (
                                    <Button classes="long-btn" onClick={goNext}>
                                        Next
                                    </Button>
                                )}
                            </div>
                        ))}
                    </Slider>
                </div>
            </ModelWrapper>
        </React.Fragment>
    )
}

// export
export default OnboardingScreens
