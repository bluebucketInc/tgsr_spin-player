// imports
import React from 'react'
import { Route, Redirect } from 'react-router-dom'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

// pages
import BoomBox from './containers/boombox'
import VoteOverlay from './containers/vote-overlay'
import OnboardingScreens from './containers/onboarding'
import { WhyVote, WinPrizes } from './containers/content-popup'

// state
import { onOnboardingComplete } from './state/reducers/player'

// app
class App extends React.Component {
    // render method
    render() {
        return (
            <React.Fragment>
                <Route
                    exact
                    path="/"
                    render={() =>
                        this.props.hasOnboarded ? (
                            <BoomBox />
                        ) : (
                            <Redirect to="/how-to-play" />
                        )
                    }
                />
                <Route
                    exact
                    path="/how-to-play"
                    render={() => (
                        <OnboardingScreens
                            onOnboardingComplete={
                                this.props.onOnboardingComplete
                            }
                        />
                    )}
                />
                <Route exact path="/vote" component={VoteOverlay} />
                <Route exact path="/why-vote" component={WhyVote} />
                <Route exact path="/how-to-win" component={WinPrizes} />
            </React.Fragment>
        )
    }
}

// mapping state to props
const mapStateToProps = ({ player }) => ({
    hasOnboarded: player.hasOnboarded,
})

// // dispatch to props
const mapDispatchToProps = dispatch =>
    bindActionCreators(
        {
            onOnboardingComplete,
        },
        dispatch
    )

// export
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(App)
