// onboarding screens
export const onBoarding = [
    {
        sub: 'CREATE YOUR  MASHUP AND VOTE',
        head: 'PICK A TIME. PICK A TUNE.',
        image: require('../assets/images/onboarding/one.png'),
        image_m: require('../assets/images/onboarding/mobile/one.png'),
    },
    {
        sub: 'CREATE YOUR MASHUP AND VOTE',
        head: 'LISTEN TO YOUR MASHUP',
        image: require('../assets/images/onboarding/two.png'),
        image_m: require('../assets/images/onboarding/mobile/two.png'),
    },
    {
        sub: 'CREATE YOUR MASHUP AND VOTE',
        head: 'VOTE FOR YOUR FAVOURITE MASHUP',
        image: require('../assets/images/onboarding/three.png'),
        image_m: require('../assets/images/onboarding/mobile/three.png'),
    },
    {
        sub: 'STAND A CHANCE TO WIN AMAZING PRIZES',
        head: `SHARE YOUR MASHUP ON SOCIAL PLATFORMS`,
        image: require('../assets/images/onboarding/four.png'),
        image_m: require('../assets/images/onboarding/mobile/four.png'),
        end: true,
    },
]
